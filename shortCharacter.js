const prompt = require("prompt-sync")();


const shortChar = (S) => {

    let str = S.toLowerCase();

    let vowels = str.match(/[aeiou]/ig);
    let consonant = str.match(/[^aeiou]/gi);

    console.log("vowels : ", ...vowels);
    console.log("consonant : ", ...consonant);
};

const input = prompt("Input One line Words ");
let result = shortChar(input);

// console.log(...result);