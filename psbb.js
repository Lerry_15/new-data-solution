const prompt = require("prompt-sync")();

const psbb = (n, m) => {

    let sum = 0;
    let hasilBagi = 0

    n = parseInt(n);
    if(m.length < n) {
        return 'Input must be equal with count of family';
    };

    arr = m.split(" ");

    for(i = 0; i < arr.length; i++) {
        sum = sum + parseInt(arr[i]);
    }

    hasilBagi = sum / 4;

    return Math.ceil(hasilBagi);
}

const nOf = prompt("Input the number of families ");
const mIf = prompt("Input the number of members in the family ");
let result = psbb(nOf, mIf);

console.log(result);